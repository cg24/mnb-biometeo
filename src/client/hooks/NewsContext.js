// context pour les notifications
import {createContext} from "react";

export const NewsContext = createContext(null);

/*// the provider avoid to write <NewsContext.provider /> for main classe
function NewsContextProvider({NewsContextValues, NewsContextSetValues, children}) {
  const {Provider} = NewsContext;
  return <Provider value={{NewsContextValues, NewsContextSetValues}}>{children}</Provider>;
}

// à utiliser dans les classes enfants qui veulent récupérer le context
function useNewsContext() {
  const {NewsContextValues, NewsContextSetValues} = useContext(NewsContext) || {};

  return {NewsContextValues, NewsContextSetValues};
}

export {useNewsContext, NewsContext, NewsContextProvider};
*/
