/*<script type="text/javascript">
var _paq = window._paq = window._paq || [];
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function() {
	var u="//matomo.dordogne.fr/";
	_paq.push(['setTrackerUrl', u+'matomo.php']);
	_paq.push(['setSiteId', '11']);
	var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
})();
</script>*/
import React, {useEffect} from "react";
import {useHistory} from "react-router-dom";
import {MatomoProvider as MtmoProvider, createInstance, useMatomo} from "@datapunt/matomo-tracker-react";

export const instance = createInstance({
  urlBase: "https://matomo.dordogne.fr",
  siteId: 11,
  heartBeat: {
    // optional, enabled by default
    active: true, // optional, default value: true
    seconds: 10 // optional, default value: `15
  },
  configurations: {
    // optional, default value: {}
    // any valid matomo configuration, all below are optional
    disableCookies: true,
    setSecureCookie: true,
    setRequestMethod: "POST"
  }
});

export function MatomoProvider({children}) {
  return (
    <MtmoProvider value={instance}>
      <MatomoRouteTracker>{children}</MatomoRouteTracker>
    </MtmoProvider>
  );
}

function MatomoRouteTracker({children}) {
  const history = useHistory();
  const {trackPageView, trackEvent} = useMatomo();

  useEffect(() => {
    return history.listen((location) => {
      trackPageView();
      //trackEvent({category: "eventtrack", action: `${location.pathname}${location.search}`});
    });
  }, [history]);

  return <>{children}</>;
}
