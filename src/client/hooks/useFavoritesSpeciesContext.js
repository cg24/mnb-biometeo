import React, {createContext, useContext} from "react";
import {func, object} from "prop-types";

//localStorage name
const _FS = "favoritespecies";
//value getter from local stoage
function getFavoritesSpecies() {
  const a = localStorage.getItem(_FS);
  return a ? JSON.parse(a) : {};
}
//new value setter for localstorage
function setFavoritesSpecies(newValue) {
  localStorage.setItem(_FS, JSON.stringify(newValue));
}

// creating the context and init it from readed value from localstorage
const FavoritesSpeciesContext = createContext(getFavoritesSpecies());

// the provider avoid to write <FavoritesSpeciesContext.provider /> for main classe
function FavoritesSpeciesContextProvider({contextFSValues, contextFSSetValues, children}) {
  const {Provider} = FavoritesSpeciesContext;
  return <Provider value={{contextFSValues, contextFSSetValues}}>{children}</Provider>;
}

// à utiliser dans les classes enfants qui veulent récupérer le context
function useFavoritesSpeciesContext() {
  const {contextFSValues, contextFSSetValues} = useContext(FavoritesSpeciesContext) || {};

  return {contextFSValues, contextFSSetValues};
}

export {
  useFavoritesSpeciesContext,
  FavoritesSpeciesContext,
  FavoritesSpeciesContextProvider,
  getFavoritesSpecies,
  setFavoritesSpecies
};

FavoritesSpeciesContextProvider.propTypes = {
  contextFSSetValues: func.isRequired,
  contextFSValues: object.isRequired
};
