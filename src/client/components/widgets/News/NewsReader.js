import React, {useState, useContext} from "react";
import {makeStyles} from "@material-ui/core/styles";

import clsx from "clsx";
import {Grid, Box, IconButton} from "@material-ui/core";

import FitText from "@kennethormandy/react-fittext";
import {NewsContext} from "../../../hooks/NewsContext";
import Config from "../../../Config";
import {MyIcon} from "../MyIcons";
import {useQuery} from "@apollo/react-hooks";
import {gqlNews} from "./news.gql.js";

import globalStyles from "../../../globalStyles";
import {News} from "./News";
import {useMediaQueries} from "../../../utilities/responsive";
import cercleBackPng from "../../../public/icons/cercle-back-x1.png";
import cercleNextPng from "../../../public/icons/cercle-next-x1.png";
import {ShareOnSocial, createTextForTweet} from "../Categorie/ShareOnSocial";
import sharebluePng from "../../../public/icons/share-blue.png";
import {_readReadedNews, _writeReadedNews, _addReadedNews, _isThisNewsReaded} from "./readedNews";
import styled from "styled-components";

const ContainerDiv = styled("div")`
  background-color: ${Config.colors.white};
  color: ${Config.colors.blue};
  margin: auto;
  padding: 4vw;
  padding: min(4vw, 30px);
  padding-top: 15px;
  padding-bottom: 15px;
  border-radius: 10px;
`;

const ContainerMobDiv = styled("div")`
  background-color: ${Config.colors.white};
  color: ${Config.colors.blue};
  margin: auto;
  padding: 4vw;
  border-radius: 10px;
`;

const useStyles = makeStyles((theme) => ({
  center: {display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center"},

  mr16: {
    marginRight: "16px"
  },
  pB40: {
    paddingBottom: 40
  },
  font20: {
    fontSize: 20,
    fontFamily: "Selawik Light !important"
  },
  font16: {
    fontSize: 16,
    fontFamily: "Selawik Light !important"
  },

  ...globalStyles
}));

/**
 * affiche les news (twit) pour une ville,
 * @param {*} param0
 */
export default function NewsReader({city}) {
  const classes = useStyles();

  const {isDesktop} = useMediaQueries();
  const [allNews, setAllNews] = useState([]);
  const [newsIndex, setNewsIndex] = useState(0);
  const iconSize = Config.iconSize.desktop;
  const {newsContextValue, newsContextSetValue} = useContext(NewsContext);

  function nextNews() {
    if (allNews?.length > newsIndex + 1) {
      _addReadedNews(allNews?.[newsIndex]);
      setNewsIndex(newsIndex + 1);
      setUnreadNews(allNews);
    }
  }

  function previousNews() {
    if (newsIndex > 0) {
      // si c'est la dernière on la marque comme lu sinon on ne pourra jamais le faire avec le bouton next qui est desactivé pour la dernière
      if (allNews?.length === newsIndex + 1) {
        _addReadedNews(allNews?.[newsIndex]);
        setUnreadNews(allNews);
      }
      setNewsIndex(newsIndex - 1);
    }
  }

  useQuery(gqlNews, {
    variables: {geonamesId: city.id},
    onCompleted: (data) => {
      let notifications = data?.news?.notifications /*.reverse()*/ || [];
      setAllNews(notifications);
      setUnreadNews(notifications);
    }
  });

  function setUnreadNews(all_news) {
    let unreaded = 0;
    for (let i in all_news) {
      if (!_isThisNewsReaded(all_news?.[i])) {
        unreaded++;
      }
    }
    newsContextSetValue(unreaded);
  }

  const showShareTweet = window.location.origin.includes("dev-mnb") || window.location.origin.includes("localhost");

  function renderContent() {
    return (
      <>
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="stretch"
          id={"actu"}
          className={clsx(classes.fs40, classes.segoeSemiBold, classes.pB40)}>
          <Grid item xs={6} lg={8}>
            <FitText compressor={1} minFontSize={20} maxFontSize={35}>
              Actualités
            </FitText>
          </Grid>
          <Grid item xs={4} lg={3} className={clsx(classes.font16, classes.flexCenter)}>
            {!_isThisNewsReaded(allNews?.[newsIndex]) && <i>Non lu</i>}
          </Grid>
          <Grid item xs={2} lg={1} className={clsx(classes.font16, classes.flexCenter)}>
            {showShareTweet && (
              <ShareOnSocial configText={createTextForTweet(allNews?.[newsIndex])}>
                {MyIcon(sharebluePng, iconSize.width, iconSize.maxWidth)}{" "}
              </ShareOnSocial>
            )}
          </Grid>
        </Grid>
        <div className={classes.flexColumnCC}>
          <div className={classes.width100}>
            <News item={allNews?.[newsIndex]} isDesktop={isDesktop} addContainer={false} />
          </div>
          {allNews.length > 1 && (
            <Grid container direction="row" justify="center" alignItems="stretch">
              <Grid item xs={2} lg={1}>
                <Box display="flex" alignItems="center" justifyContent="center">
                  {newsIndex > 0 && (
                    <IconButton onClick={() => previousNews()}>{MyIcon(cercleBackPng, "8vw", "35px")}</IconButton>
                  )}
                </Box>
              </Grid>
              <Grid item xs={4} lg={2} className={clsx(classes.center, classes.font20)}>
                {newsIndex + 1 + " / " + allNews.length}
              </Grid>
              <Grid item xs={2} lg={1}>
                <Box display="flex" alignItems="center" justifyContent="center">
                  {allNews?.length > newsIndex + 1 && (
                    <IconButton onClick={() => nextNews()}>{MyIcon(cercleNextPng, "8vw", "35px")}</IconButton>
                  )}
                </Box>
              </Grid>
            </Grid>
          )}
        </div>
      </>
    );
  }

  if (allNews && allNews.length > 0) {
    return isDesktop ? (
      <ContainerDiv>{renderContent()}</ContainerDiv>
    ) : (
      <ContainerMobDiv>{renderContent()}</ContainerMobDiv>
    );
  } else return null;
}
