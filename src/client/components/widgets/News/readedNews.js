//localStorage name
const _RN = "readednews";

//value getter from local stoage
export function _readReadedNews() {
  const a = localStorage.getItem(_RN);
  return a ? JSON.parse(a) : {};
}

//new value setter for localstorage
export function _writeReadedNews(newValue) {
  localStorage.setItem(_RN, JSON.stringify(newValue));
}

export function _addReadedNews(news) {
  let r = _readReadedNews();
  r[news?.id] = true;
  _writeReadedNews(r);
}

export function _isThisNewsReaded(news) {
  const allNews = _readReadedNews();
  return !!allNews?.[news.id];
}
