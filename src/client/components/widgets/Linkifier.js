import React from "react";

import {startsWithOneOf, replaceAll} from "../../utilities/tools";
import Linkify from "react-linkify";
/**
 * take a string text, and parse it, for each link display a <a href> node
 * @param {*} content
 */
export default function Linkifier({content} = {}) {
  return (
    <Linkify
      componentDecorator={(decoratedHref, decoratedText, key) => (
        <a target="blank" href={decoratedHref} key={key}>
          {decoratedText}
        </a>
      )}>
      {content}
    </Linkify>
  );

  /*
  try {
    if (content === "") {
      return "";
    }
    // list of strating link to look for
    const matchs = ["https://", "http://", "www."];
    // list of all word
    const words = replaceAll(content, "\n", "").split(" ");

    return (
      <div>
        {words.map((item, index) => {
          if (startsWithOneOf(item, matchs)) {
            return (
              <a target="blank" href={item} key={index}>
                {item}
              </a>
            );
          }
          return <span key={index}>{item + " "}</span>;
        })}
        <span style={{color: "white"}}>custom</span>
      </div>
    );
  } catch (error) {
    console.log(error);
    return (
      <div>
        <Linkify
          componentDecorator={(decoratedHref, decoratedText, key) => (
            <a target="blank" href={decoratedHref} key={key}>
              {decoratedText}
            </a>
          )}>
          {content}
        </Linkify>
        <span style={{color: "white"}}>Linkify</span>
      </div>
    );
  }*/
}
