import React from "react";
import Config from "../../../Config";
import {withStyles} from "@material-ui/core/styles";
import Badge from "@material-ui/core/Badge";
import NotificationsIcon from "@material-ui/icons/Notifications";

const styles = (theme) => ({
  customBadge: {
    backgroundColor: Config.colors.lightBlue,
    color: Config.colors.white,
    fontSize: "18px"
  }
});

function BlueBadge(props) {
  const {classes, badgeContent} = props;
  return (
    <div>
      <Badge classes={{badge: classes.customBadge}} badgeContent={badgeContent}>
        <NotificationsIcon />
      </Badge>
    </div>
  );
}

export default withStyles(styles)(BlueBadge);
