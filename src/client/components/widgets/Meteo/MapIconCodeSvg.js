/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";
import {MySvg} from "../MyIcons";
import {useMediaQueries} from "../../../utilities/responsive";

/**
 * affiche l'icon a partir de l'iconCode retourné par l'API openweather
 * @param {*} iconCode
 * @param {*} fill
 * @param {*} stroke
 */
export function MapIconCodeSvg(iconCode, fill = "#fff", stroke = "#fff") {
  /* au aurait pu éviter d 'avoir cette fonction pour le mapping mais il n'est pas possible de faire 
      import {ReactComponent as 13d} from "../../public/icons/meteo/13d.svg";
      erreur car le nom du composant doit commencer par une lettre 
  */
  // https://openweathermap.org/weather-conditions#How-to-get-icon-URL
  const mapping = {
    "01d": "svg01d",
    "01n": "svg01n",
    "02d": "svg02d",
    "02n": "svg02d",
    "02n": "svg02n",
    "03d": "svg03d",
    "03n": "svg03d",
    "04d": "svg04d",
    "04n": "svg04d",
    "09d": "svg09d",
    "09n": "svg09d",
    "10d": "svg10d",
    "10n": "svg10n",
    "11d": "svg11d",
    "11n": "svg11d",
    "13d": "svg13d",
    "13n": "svg13d",
    "50d": "svg50d",
    "50n": "svg50d"
  };
  if (!mapping?.[iconCode]) {
    console.warn("the following icon was not found in MapIconCodeSvg", iconCode);
  }
  let svgfilename = mapping?.[iconCode] || "01d";

  return <MySvg svgfilename={svgfilename} fill={fill} stroke={stroke} />;
}
