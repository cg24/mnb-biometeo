import React from "react";
import {Drawer as DrawerMaterialUI} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import clsx from "clsx";
import {useMediaQueries} from "../../../utilities/responsive";
import globalStyles from "../../../globalStyles";

import {SideMenuTopBar} from "./SideMenuTopBar";
import styled from "styled-components";

const useStyles = makeStyles((theme) => ({
  ...globalStyles
}));

const ContainerDiv = styled("div")`
  width: auto;
  min-width: 350px;
  min-width: min(85vw, 700px);
  max-width: 700px;
  max-width: max(35vw, 700px);
  padding-top: 10px;
  padding-bottom: 35px;
`;

const ContainerMobDiv = styled("div")`
  min-width: 85vw;
  max-width: 100vw;
`;


/**
 * Affiche une modal blanche sur tout le coté droit de la page
 *
 * @param {*}
 */
export function Drawer({show, setShow, showHorizontalPadding, children, title, showTopBar}) {
  const classes = useStyles();

  const {isDesktop} = useMediaQueries();

  function renderContent() {
    return (
      <div
        className={clsx(
          !!showHorizontalPadding && isDesktop && classes.drawerHorizontalPadding,
          !!showHorizontalPadding && !isDesktop && classes.drawerHorizontalPaddingMobile,
          classes.fullHeight
        )}
        role="presentation">
        {showTopBar && <SideMenuTopBar title={title} />}
        {children}
      </div>
    );
  }
  return (
    <DrawerMaterialUI anchor={"right"} open={show} onClose={setShow}>
      {isDesktop ? (
        <ContainerDiv>{renderContent()}</ContainerDiv>
      ) : (
        <ContainerMobDiv>{renderContent()}</ContainerMobDiv>
      )}
    </DrawerMaterialUI>
  );
}
