import React from "react";

import {makeStyles} from "@material-ui/core/styles";

import globalStyles from "../../globalStyles";

const useStyles = makeStyles((theme) => ({
  ...globalStyles
}));

export default function HorizontalSpacer(params) {
  const classes = useStyles();
  return <div className={classes.horizontalSpacer} />;
}
