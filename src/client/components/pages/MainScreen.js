/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from "react";

import {makeStyles} from "@material-ui/core/styles";
import TopMenu from "../widgets/TopMenu";
import Meteo from "../widgets/Meteo/Meteo";
import {WaterGraph} from "../widgets/Water/WaterGraph";
import {WaterDetails} from "../widgets/Water/WaterDetails";
import NewsReader from "../widgets/News/NewsReader";
import Categorie from "../widgets/Categorie/Categorie";
import Footer from "../widgets/Footer";

import HorizontalSpacer from "../widgets/HorizontalSpacer";
import Config from "../../Config";

import packageVersion from "../../../../package.json";
// don't desctruture in import directly for package.json => warning
const {version} = packageVersion;

const useStyles = makeStyles((theme) => ({
  version:{
    color: Config.colors.darkBlue,
    fontSize: "16px",
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-end",
    margin : "10px",
    marginTop: "0px"
  }
}));

/***
 * affiche la page principale avec la barre du haut, la météo, catégérories etc
 */
export function MainScreen({categoriesData, meteoData, waterData, city}) {
  const classes = useStyles();

  return (
    <div>
      <Categorie
        name="air"
        data={categoriesData?.taxonsAir?.edges}
        noPaddingTop={true}
        bgpos="bottom"
        topChild={
          <>
            <TopMenu />
            <Meteo meteoData={meteoData} />
          </>
        }
      />

      <Categorie
        name="sol"
        data={categoriesData?.taxonsSol?.edges}
        bgpos="center"
        bottomChild={<WaterDetails city={city} waterData={waterData} />}
      />

      <Categorie
        name="eau"
        data={categoriesData?.taxonsEau?.edges}
        noPaddingBottom={true}
        bgpos="top"
        bottomChild={
          <>
            <WaterGraph city={city} waterData={waterData} waterData={waterData} />
            <HorizontalSpacer />
            <NewsReader city={city} />
            <HorizontalSpacer />
            <Footer />
          </>
        }
      />
      <div className={classes.version}>{"version " + version}</div>
    </div>
  );
}
