/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useState} from "react";
import PropTypes from "prop-types";
import classnames from "clsx";
import {makeStyles} from "@material-ui/core/styles";
import {useSnackbar} from "notistack";
import {Collapse, Paper, Typography, Card, CardActions, IconButton} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles((theme) => ({
  card: {
    maxWidth: 600,
    minWidth: 344
  },
  typography: {
    color: theme.palette.error.contrastText,    
    flex: 1
  },
  actionRoot: {
    display: "flex",
    padding: "8px 8px 8px 16px",
    backgroundColor: theme.palette.error.main
  },
  icons: {
    marginLeft: "auto"
  },
  expand: {
    padding: theme.spacing(1),
    transform: "rotate(0deg)",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  collapse: {
    padding: theme.spacing(2),
    overflow: "scroll",
    fontSize: theme.typography.fontSize * 0.9
  },
  checkIcon: {
    fontSize: 20,
    color: "#b3b3b3",
    paddingRight: 4
  },
  button: {
    padding: 0,
    textTransform: "none"
  }
}));

export const SnackErrorMessage = React.forwardRef(({id, message, error}, ref) => {
  const classes = useStyles();
  const {closeSnackbar} = useSnackbar();
  const [expanded, setExpanded] = useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  // ne marche pas
  const handleDismiss = () => {
    console.log("handleDismiss", ref, id);
    closeSnackbar(id);
    // closeSnackbar(ref);
  };

  return (
    <Card className={classes.card} ref={ref}>
      <CardActions classes={{root: classes.actionRoot}}>
        <Typography variant="subtitle2" className={classes.typography}>
          {message.toUpperCase()}
        </Typography>
        <div className={classes.icons}>
          <IconButton
            aria-label="Show more"
            className={classnames(classes.expand, {[classes.expandOpen]: expanded})}
            onClick={handleExpandClick}>
            <ExpandMoreIcon />
          </IconButton>

          <IconButton className={classes.expand} onClick={handleDismiss}>
            <CloseIcon />
          </IconButton>
        </div>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <Paper className={classes.collapse}>
          {error}
        </Paper>
      </Collapse>
    </Card>
  );
});

SnackErrorMessage.propTypes = {
  id: PropTypes.number.isRequired
};
