import React from "react";

import {SnackErrorMessage} from "./SnackErrorMessage";
import {withSnackbar} from "notistack";

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {errorMessage: false};
  }

  static getDerivedStateFromError(error) {
    return {errorMessage: error.toString()};
  }

  componentDidCatch(error, info) {
    console.log(info);
    this.props.enqueueSnackbar(error.toString(), {
      content: (id) => {
        return <SnackErrorMessage id={id} message={error.toString()} error={<>{JSON.stringify(info, null, 2)}</>} />;
      },
      persist: true,
      anchorOrigin: {
        vertical: "bottom",
        horizontal: "right"
      }
    });
  }

  render() {
    if (this.state.errorMessage) {
      return <div>💥 Une erreur est survenue 💥</div>;
    }
    return this.props.children;
  }
}

export default withSnackbar(ErrorBoundary);
