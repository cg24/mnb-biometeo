import {
  GraphQLTypeConnectionQuery,
  SynaptixDatastoreSession,
  getObjectsResolver,
  PropertyFilter, LinkFilter
} from "@mnemotix/synaptix.js";
import { TaxonDefinition } from "../TaxonDefinition";
import dayjs from "dayjs";

export class TaxonGraphQLTypeConnectionQuery extends GraphQLTypeConnectionQuery {
  /**
   * @inheritdoc
   */
  generateType(modelDefinition) {
    const graphQLType = modelDefinition.getGraphQLType();
    return this._wrapQueryType(`
      """
       This service returns a random list of taxons filtered by following parameters :
       
       Parameters :
         - geonamesId: [REQUIRED] Geonames place id.
         - area: [Optional] Taxon area to filter on (Eau, Sol, Air)
         - seed: [Optional] Use it for testing purposes to test results controlled randomness
         - taxonId: [Optional] Use it to force taxon presence in the top of the list
      """
      ${this.generateFieldName(
        modelDefinition
      )}(geonamesId:ID! area: TaxonArea seed:Int taxonId:ID): ${graphQLType}Connection
    `);
  }

  /**
   * @inheritdoc
   */
  generateResolver(modelDefinition) {
    return this._wrapQueryResolver({
      [this.generateFieldName(modelDefinition)]:
        /**
         * @param _
         * @param {string} geonamesId
         * @param {string} area
         * @param {number} seed
         * @param {string} taxonId
         * @param {SynaptixDatastoreSession} synaptixSession
         * @param {object} info
         */
        async (_, { geonamesId, area, seed, taxonId }, synaptixSession, info) => {
          let propertyFilters = [];

          let linkFilters = [
            new LinkFilter({
              linkDefinition: TaxonDefinition.getLink("hasOccurrence"),
              any: true
            }),
            new LinkFilter({
              linkDefinition: TaxonDefinition.getLink("hasPicture"),
              any: true
            })
          ];

          if (area) {
            propertyFilters.push(
              new PropertyFilter({
                propertyDefinition: TaxonDefinition.getProperty("area"),
                value: area
              })
            );
          }

          let featuredTaxon;

          if(taxonId){
            const featuredTaxons = await synaptixSession.getIndexService().getNodes({
              modelDefinition: TaxonDefinition,
              limit: 1,
              idsFilters: [taxonId],
              propertyFilters,
              linkFilters
            });

            featuredTaxon = featuredTaxons[0];
          }


          /**
           * Explanation how to fullfill backlog requirement.
           * @see https://mnemotix.atlassian.net/secure/RapidBoard.jspa?rapidView=45&projectKey=MNB24&modal=detail&selectedIssue=MNB24-72
           *
           * We use the "function_score" query of ES with a controlled "random_score" seed computed by the concatenation of :
           *
           * - Current date at format YYYYMMDD
           * - Geonames numbered ID.
           *
           * Ex : 202007026429478
           *
           * Seed results to be a 15 digits number.
           */
          const taxons = await synaptixSession.getIndexService().getNodes({
            modelDefinition: TaxonDefinition,
            limit: !!featuredTaxon ? 2 : 3,
            propertyFilters,
            linkFilters,
            getRootQueryWrapper: ({ query }) => ({
              "function_score": {
                "query": {
                  "constant_score": {
                    "filter": query
                  }
                },
                "random_score": {
                  "seed": seed || parseInt(`${dayjs().format("YYYYMMDD")}${geonamesId.replace(/[^0-9]*/g, '')}`),
                  "field": "_seq_no"
                }
              }
            })
          });

          if (featuredTaxon){
            taxons.unshift(featuredTaxon);
          }

          return synaptixSession.wrapObjectsIntoGraphQLConnection(taxons, { limit: 3 });
        }
    });
  }
}
