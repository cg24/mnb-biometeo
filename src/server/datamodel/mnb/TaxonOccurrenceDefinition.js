import {
  LiteralDefinition,
  ModelDefinitionAbstract,
  FilterDefinition,
  SortingDefinition
} from "@mnemotix/synaptix.js";
import { TaxonOccurrenceGraphQLDefinition } from "./graphql/TaxonOccurrenceGraphQLDefinition";

export class TaxonOccurrenceDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnb:TaxonOccurrence";
  }

  /**
   * @return {string}
   */
  static getIndexType() {
    return "occurrence";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return TaxonOccurrenceGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      new LiteralDefinition({
        literalName: "gbifTaxonKey",
        description:
          "Taxon GBIF key also known as P846 property from wikidata entity",
        rdfDataProperty: "mnb:gbifTaxonKey"
      }),
      new LiteralDefinition({
        literalName: "resultTime",
        description: "Taxon varnacular name",
        rdfDataProperty: "http://www.w3.org/ns/sosa/resultTime",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#dateTime"
      }),
      new LiteralDefinition({
        literalName: "primarySource",
        description: "Primary source web page",
        rdfDataProperty: "http://www.w3.org/ns/prov#hadPrimarySource"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getFilters() {
    return [
      ...super.getFilters(),
      new FilterDefinition({
        filterName: "closestFromGeoCoords",
        indexFilter: ({ lat, lon, distance }) => ({
          geo_distance: {
            distance: distance || "500km",
            hasGeometry: { lat, lon }
          }
        }),
        indexScriptFields: ({ lat, lon }) => ({
          distance: {
            script: {
              lang: "painless",
              source: `doc['hasGeometry'].arcDistance(${lat}, ${lon})`
            }
          }
        })
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getSortings() {
    return [
      new SortingDefinition({
        sortingName: "closestFromGeoCoords",
        indexSorting: ({ lat, lon, direction, unit, mode, distanceType }) => ({
          _geo_distance: {
            hasGeometry: {
              lat,
              lon
            },
            order: direction || "asc",
            unit: unit || "km",
            mode: mode || "min",
            distance_type: distanceType || "arc",
            ignore_unmapped: true
          }
        })
      })
    ];
  }
}
