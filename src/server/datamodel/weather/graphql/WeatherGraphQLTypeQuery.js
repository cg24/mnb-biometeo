import {GraphQLTypeQuery} from "@mnemotix/synaptix.js";
import {weatherClient} from "../../../services/OpenWeatherClient";
import {atmoNaClient} from "../../../services/AtmoNaClient";

export class WeatherGraphQLTypeQuery extends GraphQLTypeQuery{
  /**
   * @inheritdoc
   */
  generateType(modelDefinition) {
    const graphQLType = modelDefinition.getGraphQLType();
    return this._wrapQueryType(`
      """
       This service returns a weather situation for a geonames place.
       
       Parameters :
         - geonamesId: [REQUIRED] Geonames place id.
      """
      ${this.generateFieldName(modelDefinition)}(geonamesId:ID!): ${graphQLType}
    `);
  }

  /**
   * @inheritdoc
   */
  generateResolver(modelDefinition) {
    return this._wrapQueryResolver({
      [this.generateFieldName(modelDefinition)]: async (_, {geonamesId}) => {
        let weather = await weatherClient.getCurrentWeatherForGeonamesId({geonamesId});
        let atmo    = await atmoNaClient.getCurrentAtmoIndices({});

        weather.atmo = atmo;

        return weather;
      }
    });
  }
}