import {GraphQLTypeDefinition, GraphQLProperty} from "@mnemotix/synaptix.js";

import dayjs from "dayjs";
import {WeatherGraphQLTypeQuery} from "./WeatherGraphQLTypeQuery";

export class WeatherGraphQLDefinition extends GraphQLTypeDefinition {
  /**
   * @inheritDoc
   */
  static getTypeQuery(){
    return new WeatherGraphQLTypeQuery();
  }

  /**
   * @inheritDoc
   */
  static getExtraProperties() {
    return [
      new GraphQLProperty({
        name: "icon",
        description: `
          This is the weather icon code following.
          @see https://openweathermap.org/weather-conditions to get more information.
        `,
        type: "String",
        typeResolver: object => {           
          return object.weather?.[0]?.icon
        }
      }),
      new GraphQLProperty({
        name: "humidity",
        description: `
          This is the humidity in %
        `,
        type: "String",
        typeResolver: object => object.humidity
      }),
      new GraphQLProperty({
        name: "temperature",
        description: `
          This is the temperature in Celsius
        `,
        type: "String",
        typeResolver: object => object.temp
      }),
      new GraphQLProperty({
        name: "temperatureTrend",
        description: `
          This is the temperature trend (increasing|decreasing)
        `,
        type: "String",
        typeResolver: object => object.temperatureTrend
      }),
      new GraphQLProperty({
        name: "sunrise",
        description: `
          This is the sunrise datetime
        `,
        type: "String",
        typeResolver: object => dayjs(object.sunrise * 1000).toISOString()
      }),
      new GraphQLProperty({
        name: "sunset",
        description: `
          This is the sunset datetime
        `,
        type: "String",
        typeResolver: object => dayjs(object.sunset * 1000).toISOString()
      }),
      new GraphQLProperty({
        name: "dayTimeDiff",
        description: `
          This is the day time diffence in minutes
        `,
        type: "Int",
        typeResolver: object => object.dayTimeDiff
      }),
      new GraphQLProperty({
        name: "windOrientation",
        description: `
          This is the wind orientation
        `,
        type: "String",
        typeResolver: object => object.wind_deg
      }),
      new GraphQLProperty({
        name: "windSpeed",
        description: `
          This is the wind speed in meter/sec
        `,
        type: "String",
        typeResolver: object => object.wind_speed
      }),
      new GraphQLProperty({
        name: "pressure",
        description: `
          This is the pressure in hPa
        `,
        type: "String",
        typeResolver: object => object.pressure
      }),
      new GraphQLProperty({
        name: "uvIndex",
        description: `
          This is the UV index 
        `,
        type: "String",
        typeResolver: object => object.uvi
      }),
      new GraphQLProperty({
        name: "uvIndexTrend",
        description: `
          This is the UV index trend (increasing|decreasing)
        `,
        type: "String",
        typeResolver: object => object.uvIndexTrend
      }),
      new GraphQLProperty({
        name: "forcast",
        description: `
          This the weather forcast
        `,
        type: "[WeatherForcast]",
        typeResolver: object => object.forcast
      }),
      new GraphQLProperty({
        name: "atmoIndex",
        description: `
          This the atmosphere index in %
        `,
        type: "Int",
        typeResolver: object => object.atmo.current
      }),
      new GraphQLProperty({
        name: "atmoNextDayIndex",
        description: `
          This the atmosphere index in %
        `,
        type: "Int",
        typeResolver: object => object.atmo.nextDay
      }),
      new GraphQLProperty({
        name: "atmoTrend",
        description: `
          This the atmosphere trend (stable|increasing|decreasing)
        `,
        type: "String",
        typeResolver: object => object.atmo.trend
      }),
      new GraphQLProperty({
        name: "atmoNextDayTrend",
        description: `
          This the atmosphere next day trend (stable|increasing|decreasing)
        `,
        type: "String",
        typeResolver: object => object.atmo.nextDayTrend
      }),
      new GraphQLProperty({
        name: "atmoStationLabel",
        description: `
          This the atmosphere station label
        `,
        type: "String",
        typeResolver: object => "Périgueux"
      }),
      new GraphQLProperty({
        name: "atmoStationUrl",
        description: `
          This the atmosphere station URI to get more details
        `,
        type: "String",
        typeResolver: object => "https://www.atmo-nouvelleaquitaine.org/monair/commune/24322"
      })
    ];
  }
}