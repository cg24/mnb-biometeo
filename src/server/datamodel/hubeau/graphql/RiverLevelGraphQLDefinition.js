import {
  GraphQLTypeDefinition,
  GraphQLProperty,
} from "@mnemotix/synaptix.js";

export class RiverLevelGraphQLDefinition extends GraphQLTypeDefinition {}


/**
 case "Day":
 roundedDate = dayjs(date_obs).format("YYYY-MM-DD");
 break;
 case "Week":
 roundedDate = dayjs(date_obs).format("YYYY-MM (ww)");
 break;
 case "Month":
 roundedDate = dayjs(date_obs).format("YYYY-MM");
 break;
 */