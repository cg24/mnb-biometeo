import {
  ModelDefinitionAbstract,
} from "@mnemotix/synaptix.js";
import {GroundWaterObservationGraphQLDefinition} from "./graphql/GroundWaterObservationGraphQLDefinition";

export class GroundWaterObservationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getNodeType() {
    return "GroundWaterObservation";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GroundWaterObservationGraphQLDefinition;
  }
}
