import { fr as SynaptixClientToolkitFrLocale } from '@mnemotix/synaptix-client-toolkit/lib/locales/fr';

export const fr = {
  ...SynaptixClientToolkitFrLocale
}
