import jestFetchMock from 'jest-fetch-mock';

export function setupFetchMock() {
  global.originalFetch = global.fetch;
  global.mockFetch = jestFetchMock;
  global.fetch = global.mockFetch;
}
